# Building the Brown Dog Command-Line Interface for Windows

These are the instructions for building the Brown Dog installer that is end-user installable on Windows. This installer is packaged with the Python runtime and dependencies, for independent installation on the operating system. The installer method is not recommended for use in Python code, as a Brown Dog client library or module. For that type of use, try the pip install command.

This build works on Ubuntu/Debian/Linux/Mac OSX, so you do not need to run MS Windows to build for the platform.

## PyNsist: Python NullSoft Installer

The distribution for Windows uses a NullSoft Installer (NSIS), which creates an executable (.exe) that installs all necessary components, including Python. It also includes an uninstaller. After install, a "bd.exe" is placed in the BrownDog folder and can be run as "bd" from the Windows command-line. The enclosing "bin" folder may need to be added to the PATH system variable.

### Steps to Build

1. Install the NSIS package (NullSoft Installer System).
```
$ sudo apt-get install nsis       (on Ubuntu)
```
```
$ brew install makensis           (on OS X)
```
2. Install the pynsist and browndog Python modules.
```
$ sudo pip install pynsist browndog
```
3. Clone the bdcli repository.
```
$ git clone https://opensource.ncsa.illinois.edu/bitbucket/scm/bd/bdcli.git
$ cd bdcli
```
4. If this is not your first try, clean out the build and dist folders.
```
$ rm -rf build dist
```
5. Download the numpy wheel for Windows 32bit and include it in pynsist_pkgs:
 * Get wheel at https://pypi.python.org/pypi/numpy. The file will be named something like "numpy-1.13.0-cp27-none-win32.whl".
 * Unzip the wheel file and place numpy directory in pynsist_pkgs:
```
    $ mkdir pynsist_pkgs        (under bdcli checkout folder)
    $ unzip -d /tmp ~/Downloads/numpy-*-win32.whl
    $ mv /tmp/numpy pynsist_pkgs/       (numpy module folder only)
```
6. Run a local install to gather dependencies locally:
```
    $ sudo pip install .
```
7. Run the Pynsist build tool.
```
    $ pynsist pynsist_install.cfg
```
8. Find Windows installer "BrownDogCLI_1.0.exe" in the "build/nsis" folder. This is the installer executable. If you are working on a software release, please upload and link to this file in an appropriate download location.
