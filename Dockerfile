FROM ubuntu:16.04
MAINTAINER Bing Zhang <bing@illinois.edu>
USER root

# Create user: bdcli, passwd: bdcli, with sudo privilege
RUN useradd -s /bin/bash bdcli && echo "bdcli:bdcli" | chpasswd && adduser bdcli sudo

# Install necessary tools
RUN apt-get update && apt-get install -y \
    jq \
    vim \
    sudo \
    wget \
    python-pip \
    python-dev \
    build-essential \
    supervisor

# Copy files
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY MANIFEST.in setup.py sample.jpg sample.txt /home/bdcli/
COPY bdcli /home/bdcli/bdcli

# Change owner and group
RUN mkdir -p /home/bdcli/supervisor \
    && chown -R bdcli /home/bdcli \
    && chgrp -R bdcli /home/bdcli

# Set workdir
WORKDIR "/home/bdcli"

# Install BD-CLI
RUN python setup.py install

# Set path of bd commands
ENV TERM=linux

# Start pid 1
CMD ["/usr/bin/supervisord", "-n"]
